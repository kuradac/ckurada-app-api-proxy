# ckurada-app-api-proxy

NGINX proxy for testing terraform

## Usage

### Environment variables
* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests (default: `9000`)